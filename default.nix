with import <nixpkgs> {};

stdenv.mkDerivation {

  name = "ConTeXtTools";

  src = ./.;

  buildInputs = with python37Packages ; [
    python37
    python37Packages.inotify-simple
  ] ;

}
