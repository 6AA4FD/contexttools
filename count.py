#!/usr/bin/env python3

import re
import sys
Body = sys.stdin.read()
Body = re.sub(r'\n\\environment.*?\n', '\n', Body)
Body = re.sub(r'\n+', ' ', Body)
Body = re.sub(r'\\\w*', '', Body)
Body = re.sub(r'\[.*?\]', '', Body)
Body = re.sub(r'\{.*?\}', '', Body)
Body = re.sub(r'[\.\,\?]', '', Body)

Body = re.split(r'[ ]', Body)
Body = [x for x in Body if x]

print(str(len(Body)) + " words (does not include work titles or author names printed with \cite)")
print(str(round(len(Body)/250, 1)) + " manuscript pages")
